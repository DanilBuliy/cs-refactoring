import math
import unittest
from unittest.mock import patch
from io import StringIO
import sys
from task_02 import main


class TestCalculator(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="45\n")
    def test_main_valid_input(self, mock_input, mock_output):
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 0)

        result = mock_output.getvalue().strip()

        self.assertIn("Z1", result)
        self.assertIn("Z2", result)
        self.assertIn("Z3", result)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=['d', '\n'])
    def test_main_invalid_input(self, mock_input, mock_output):
        # Викликаємо функцію main
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 1)

        result = mock_output.getvalue().strip()
        self.assertIn("Некоректні дані. Введіть числове значення для кута a.", result)



if __name__ == '__main__':
    unittest.main()
