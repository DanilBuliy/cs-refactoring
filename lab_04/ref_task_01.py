import math
import sys

class Square:
    diagonal = None
    square = None

    def input_diagonal(self):
        self.diagonal = self.is_validated_float(input('Введіть діагональ квадрату:'))

    def is_validated_float(self,par):
        try:
            return float(par)
        except ValueError:
            print("Введене значення не є числом.")
            sys.exit(1)
    def validate(self):
        if self.diagonal <= 0:
            print(f"Довжина діагоналі ({self.diagonal}) не може бути менше або дорівнювати 0.")
            sys.exit(1)

    def square_calculate(self):
        self.square = self.diagonal ** 2 / 2

    def print_square_area(self):
        print(f"Площа квадрата з діагоналлю {self.diagonal} дорівнює {self.square:.10f}.")
        sys.exit(0)

    def run_process(self):
        self.input_diagonal()
        self.validate()
        self.square_calculate()
        self.print_square_area()

def main():
    calculator = Square()
    calculator.run_process()


if __name__ == '__main__':
    main()
