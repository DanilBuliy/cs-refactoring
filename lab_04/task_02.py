import math
import sys

class App_z:
    a = None; z1 = None; z2 = None; z3 = None; determ = None

    def input(self):
        try:
            self.a = float(input("Введіть значення кута a в градусах: "))
            self.a = math.radians(self.a)
        except ValueError:
            print("Некоректні дані. Введіть числове значення для кута a.")
            sys.exit(1)

    def calculation(self):
        n = math.sin(self.a) + 2 * math.sin(self.a - 10)
        self.determ = 2 * math.cos(self.a) - math.sin(3 * self.a)

        self.z1 = n / self.determ
        self.z2 = math.tan(3 * self.a) / self.z1
        self.z3 = self.z1 + self.z2
    def validation(self):
        error_message = None

        if self.determ == 0:
            error_message="Некоректні дані. Знаменник не може дорівнювати нулю."

        if not isinstance(self.z1, float) or self.z1 == 0:
            error_message="Некоректні дані. Значення Z1 повинно бути числом і не може дорівнювати нулю."

        if not isinstance(self.z1, float) or not isinstance(self.z2, float):
            error_message="Некоректні дані. Значення Z1 та Z2 повинні бути числами."

        if error_message:
            print(error_message)
            sys.exit(1)
    def print_data(self):
        print(f"Z1 = {self.z1}")
        print(f"Z2 = {self.z2}")
        print(f"Z3 = {self.z3}")
        sys.exit(0)

def main():
    obj_app_z = App_z()
    obj_app_z.input()
    obj_app_z.calculation()
    obj_app_z.validation()
    obj_app_z.print_data()


if __name__ == "__main__":
    main()
