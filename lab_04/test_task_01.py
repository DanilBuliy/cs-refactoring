import unittest
from unittest.mock import patch
from io import StringIO
import sys
from task_01 import main

class TestSquareCalculator(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="4\n")
    def test_main_valid_input(self, mock_input, mock_output):
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 0)

        result = mock_output.getvalue().strip()
        expected_result = "Площа квадрата з діагоналлю 4.0 дорівнює 8.0000000000."
        self.assertIn(expected_result, result)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="0\n")
    def test_main_invalid_input(self, mock_input, mock_output):
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 1)

        result = mock_output.getvalue().strip()
        expected_result = "Довжина діагоналі (0.0) не може бути менше або дорівнювати 0."
        self.assertIn(expected_result, result)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="abc\n")
    def test_main_invalid_input_not_a_number(self, mock_input, mock_output):
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 1)

        result = mock_output.getvalue().strip()
        expected_result = "Введене значення не є числом."
        self.assertIn(expected_result, result)



if __name__ == '__main__':
    unittest.main()
