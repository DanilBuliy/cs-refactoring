import math
import sys

class App_z:
    a = None; z1 = None; z2 = None; z3 = None; determ = None

    def input(self):
        self.a = self.is_validated_float(input("Введіть значення кута a в градусах: "))

    def is_validated_float(self,par):
        try:
            return float(par)
        except ValueError:
            print("Некоректні дані. Введіть числове значення для кута a.")
            sys.exit(1)

    def set_radians(self):
        self.a = math.radians(self.a)

    def calculation(self):
        n = math.sin(self.a) + 2 * math.sin(self.a - 10)
        self.determ = 2 * math.cos(self.a) - math.sin(3 * self.a)

        self.z1 = n / self.determ
        self.z2 = math.tan(3 * self.a) / self.z1
        self.z3 = self.z1 + self.z2

    def print_data(self):
        print(f"Z1 = {self.z1}")
        print(f"Z2 = {self.z2}")
        print(f"Z3 = {self.z3}")
        sys.exit(0)

    def run_process(self):
        self.input()
        self.set_radians()
        self.calculation()
        self.print_data()

def main():
    obj_app_z = App_z()
    obj_app_z.run_process()


if __name__ == "__main__":
    main()
