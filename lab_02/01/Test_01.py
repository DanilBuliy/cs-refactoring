import unittest
from unittest.mock import patch
from io import StringIO
import sys
from ref_task_01 import main  # Підключіть ваш модуль з класом Square


class TestSquareCalculator(unittest.TestCase):
    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="4\n")
    def test_main_valid_input(self, mock_input, mock_output):
        # Викликаємо функцію main
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 0)

        result = mock_output.getvalue().strip()
        self.assertEqual("Площа квадрата з діагоналлю 4.0 дорівнює 8.000000000000002.", result)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="0\n")
    def test_main_invalid_input(self, mock_input, mock_output):
        # Викликаємо функцію main
        with self.assertRaises(SystemExit) as cm:
            main()

        self.assertEqual(cm.exception.code, 1)

        result = mock_output.getvalue().strip()
        self.assertIn("Довжина діагоналі (0.0) не може бути менше або дорівнювати 0.", result)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', return_value="abc\n")
    def test_main_invalid_input_not_a_number(self, mock_input, mock_output):
        # Викликаємо функцію main
        with self.assertRaises(SystemExit) as cm:
            main()

        # Перевіряємо, чи функція викликає sys.exit(1) для неправильного вводу
        self.assertEqual(cm.exception.code, 1)

        # Перевіряємо результат виведення
        result = mock_output.getvalue().strip()
        self.assertEqual("Введене значення не є числом.", result)


if __name__ == '__main__':
    unittest.main()
