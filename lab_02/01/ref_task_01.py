import math
import sys

class Square:
    def __init__(self):
        self.v1 = None
        self.v2 = None

    def input_diagonal(self):
        try:
            self.v1 = float(input('Введіть діагональ квадрату:'))
        except ValueError:
            print('Введене значення не є числом.')
            sys.exit(1)

    def compute(self):
        if self.v1 <= 0:
            print(f"Довжина діагоналі ({self.v1}) не може бути менше або дорівнювати 0.")
            sys.exit(1)
        else:
            self.v2 = math.sqrt(self.v1 ** 2 / 2) * math.sqrt(self.v1 ** 2 / 2)
            print(f"Площа квадрата з діагоналлю {self.v1} дорівнює {self.v2}.")
            sys.exit(0)

def main():
    calculator = Square()
    calculator.input_diagonal()
    calculator.compute()


if __name__ == '__main__':
    main()
