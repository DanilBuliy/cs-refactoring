import unittest
from unittest.mock import patch
from io import StringIO
from ref_task_02 import main

class TestFunction(unittest.TestCase):

    @patch('sys.stdin', StringIO('3\n6\n'))  # Введення прикладу даних
    @patch('sys.stdout', new_callable=StringIO)
    def test_input_num_works(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)
        expected_output = "Введіть значення x: Введіть значення y: Z1 = 29.99724740975918\nZ2 = -5.001321660645168\nZ3 = 3.998392578328894\n"
        self.assertEqual(stdout.getvalue(), expected_output)

    @patch('sys.stdin', StringIO('d\nd\n'))  # Введення прикладу даних
    @patch('sys.stdout', new_callable=StringIO)
    def test_input_non_num(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        expected_output = "Введіть значення x: Некоректні дані. Введіть числові значення для x та y.\n"
        self.assertEqual(stdout.getvalue(), expected_output)


    @patch('sys.stdin', StringIO('3\n3\n'))  # Перевірка на нульовий знаменник
    @patch('sys.stdout', new_callable=StringIO)
    def test_validate_d_zero(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        expected_output = "Введіть значення x: Введіть значення y: Некоректні дані. Знаменник не може дорівнювати нулю.\n"
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), expected_output)

    @patch('sys.stdin', StringIO('-3.45\n40\n'))  # Перевірка правильності обрахунків
    @patch('sys.stdout', new_callable=StringIO)
    def test_calc_works(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)

        output = stdout.getvalue().strip()
        self.assertIn("Z1", output)
        self.assertIn("Z2", output)
        self.assertIn("Z3", output)

    @patch('sys.stdin', StringIO('-3.45\n40\n'))  # Перевірка обрахунків при z2 = 0
    @patch('sys.stdout', new_callable=StringIO)
    def test_calc_works(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        expected_output = "Введіть значення x: Введіть значення y: Z1 = 199.99999999986287\nZ2 = -2.097932117289691\nZ3 = 90.56537074484707\n"
        self.assertEqual(stdout.getvalue(), expected_output)


if '__name__' == "main":
    unittest.main()