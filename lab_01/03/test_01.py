import unittest
from unittest.mock import patch
from io import StringIO
from ref_task_01 import main
class TestFunc(unittest.TestCase):

    @patch('sys.stdin', StringIO('3\n5\n'))  # Введення значень для радіусу та висоти
    @patch('sys.stdout', new_callable=StringIO)
    def test_valid_input(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        expected_output = "Введіть радіус основи:Введіть висоту:Об’єм конуса з радіусом основи 3.0 та висотою 5.0 дорівнює 47.1238898038469.\n"
        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(stdout.getvalue(), expected_output)


    @patch('sys.stdin', StringIO('0\n5\n'))  # Введення нульового радіусу
    @patch('sys.stdout', new_callable=StringIO)
    def test_zero_radius(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(),"Введіть радіус основи:Введіть висоту:Довжина радіусу основи (0.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('-2\n5\n'))  # Введення від'ємного радіусу
    @patch('sys.stdout', new_callable=StringIO)
    def test_negative_radius(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        #self.assertEqual(cm_ve.exception, ValueError("Введіть радіус основи:Введіть висоту:Довжина радіусу основи (-2.0) не може бути менше або дорівнювати 0.\n"))
        output = stdout.getvalue().strip()
        expected_output = "Введіть радіус основи:Введіть висоту:Довжина радіусу основи (-2.0) не може бути менше або дорівнювати 0."
        self.assertEqual(output, expected_output)

    # @patch('sys.stdin', StringIO('-2\n5\n'))  # Введення від'ємного радіусу
    # @patch('sys.stdout', new_callable=StringIO)
    # def test_negative_radius(self, stdout):
    #     with self.assertRaises(ValueError) as cm_ve:
    #         with self.assertRaises(SystemExit) as cm:
    #             main()
    #         self.assertEqual(cm.exception.code, 1)
    #         self.assertEqual(cm_ve.exception, ("Введіть радіус основи:Введіть висоту:Довжина радіусу основи (-2.0) не може бути менше або дорівнювати 0.\n"))
    @patch('sys.stdin', StringIO('3\n-2\n'))  # Введення від'ємної висоти
    @patch('sys.stdout', new_callable=StringIO)
    def test_negative_height(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть радіус основи:Введіть висоту:Довжина висоти (-2.0) не може бути менше або дорівнювати 0.\n")

    @patch('sys.stdin', StringIO('dummy\n5\n'))  # Введення недійсного радіусу
    @patch('sys.stdout', new_callable=StringIO)
    def test_non_numeric_radius(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть радіус основи:Введене значення не є числом.\n")

    @patch('sys.stdin', StringIO('3\ndummy\n'))  # Введення недійскої висоти
    @patch('sys.stdout', new_callable=StringIO)
    def test_non_numeric_height(self, stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)
        self.assertEqual(stdout.getvalue(), "Введіть радіус основи:Введіть висоту:Введене значення не є числом.\n")

if __name__ == '__main__':
    unittest.main()