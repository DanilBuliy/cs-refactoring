import sys
import math


def validate_radius(v1):
    if v1 <= 0:
        print(f"Довжина радіусу основи ({v1}) не може бути менше або дорівнювати 0.")
        sys.exit(1)

def validate_height(v2):
    if v2 <= 0:
        print(f"Довжина висоти ({v2}) не може бути менше або дорівнювати 0.")
        sys.exit(1)

def calculate_cone_volume(v1, v2):
    return (math.pi * (v1 ** 2) * v2) / 3


def main():
    try:
        v1 = float(input('Введіть радіус основи:'))
        v2 = float(input('Введіть висоту:'))
        validate_radius(v1)
        validate_height(v2)

        v3 = calculate_cone_volume(v1, v2)
        print(f"Об’єм конуса з радіусом основи {v1} та висотою {v2} дорівнює {v3}.")
        sys.exit(0)
    except ValueError:
        print('Введене значення не є числом.')
        sys.exit(1)


if __name__ == '__main__':
    main()
