import math
import sys

def calculate_z1(x, y):
    d = (x - y) ** 7
    if d == 0:
        print("Некоректні дані. Знаменник не може дорівнювати нулю.")
        sys.exit(1)
    return ((y + math.sin(x) ** 2) / d) + 5 * y

def calculate_z2(x):
    return x ** 2 + math.cos((0.7 * x) ** -4) - 15

def calculate_z3(z1, z2):
    if z2 == 0:
        print("Некоректні дані. Значення Z2 не може бути рівним нулю.")
        sys.exit(1)
    return abs((z1 - 10) / z2)

def main():
    try:
        x = float(input("Введіть значення x: "))
        y = float(input("Введіть значення y: "))

        z1 = calculate_z1(x, y)
        z2 = calculate_z2(x)
        z3 = calculate_z3(z1, z2)

        print(f"Z1 = {z1}")
        print(f"Z2 = {z2}")
        print(f"Z3 = {z3}")
        sys.exit(0)

    except ValueError as e:
        print("Некоректні дані. Введіть числові значення для x та y.")
        sys.exit(1)

if __name__ == "__main__":
    main()
