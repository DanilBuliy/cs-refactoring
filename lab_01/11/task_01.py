import sys

'''
Обчислити площу поверхні куба, якщо задано довжину його ребра.
'''


def main():
    try:
        a = float(input("Введіть довжину ребра куба: "))
        if a > 0:
            s = 6 * a ** 2
        else:
            s = "Некоректні дані. Довжина ребра куба повинна бути додатньою."
        if isinstance(s, float):
            print(f"Площа поверхні куба: {s}")
            sys.exit(0)
        else:
            print(s)
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числове значення для довжини ребра куба.")
        sys.exit(1)


if __name__ == "__main__":
    main()
