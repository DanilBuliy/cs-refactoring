import unittest
from unittest.mock import patch,MagicMock
from io import StringIO
from task_02 import input_angle, calculate_z1, calculate_z2, calculate_z3, main,ERROR2,ERROR1

class TestAngleCalculation(unittest.TestCase):
    def test_input_angle_invalid(self):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            with patch('builtins.input', side_effect=["invalid"]):
                with self.assertRaises(SystemExit) as cm:
                    input_angle()

            self.assertEqual(cm.exception.code, 1)
            output = mock_stdout.getvalue().strip()
            self.assertIn(ERROR2, output)

    def test_calculate_z1_non_zero_divisor(self):
        result = calculate_z1(0.66666666)
        self.assertIsNotNone(result)

    def test_calculate_z2_zero_divisor(self):
        with patch('sys.stdout', new_callable=StringIO) as mock_stdout:
            with patch('sys.exit') as mock_exit:
                calculate_z2(45, 0)
                mock_exit.assert_called_with(1)

            output = mock_stdout.getvalue().strip()
            expected_output = ERROR1
            self.assertEqual(output, expected_output)

    def test_calculate_z3(self):
        result = calculate_z3(2, 3)
        self.assertEqual(result, 5)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["45"])
    def test_main(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 0)

        output = mock_stdout.getvalue().strip()
        self.assertIn("Z1", output)
        self.assertIn("Z2", output)
        self.assertIn("Z3", output)


if __name__ == '__main__':
    unittest.main()


