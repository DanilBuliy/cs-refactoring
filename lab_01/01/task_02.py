import math
import sys

ERROR1 = "Некоректні дані. Значення Z1 повинно бути числом і не може дорівнювати нулю."
ERROR2 = "Некоректні дані. Введіть числове значення для кута a."

def input_angle():
    try:
        a = float(input("Введіть значення кута a в градусах: "))
        return math.radians(a)
    except ValueError as e:
        print(ERROR2)
        sys.exit(1)

def calculate_z1(a):
    n = math.sin(a) + 2 * math.sin(a - 10)
    denominator = 2 * math.cos(a) - math.sin(3 * a)

    try:
        res = n / denominator
        return res
    except ValueError as e:
        if str(e) == ERROR1:
            sys.exit(1)
        else:
            raise


def calculate_z2(a, z1):
    if z1 != 0:
        return math.tan(3 * a) / z1
    else:
        print(ERROR1)
        sys.exit(1)

def calculate_z3(z1, z2):
    return z1 + z2

def main():
    try:
        a = input_angle()
        z1 = calculate_z1(a)
        z2 = calculate_z2(a, z1)
        z3 = calculate_z3(z1, z2)

        print(f"Z1 = {z1}")
        print(f"Z2 = {z2}")
        print(f"Z3 = {z3}")
        sys.exit(0)

    except ValueError as e:
        print(e)
        sys.exit(1)


if __name__ == "__main__":
    main()
