import unittest
from unittest.mock import patch
from io import StringIO
from task_01 import vvod, calculate_square_area, main, ERROR_MESSAGE_NEGATIVE, ERROR_MESSAGE_NON_NUMERIC

class TestSquareAreaCalculator(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["-2"])
    def test_main_negative_diagonal(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()
        expected_output = ERROR_MESSAGE_NEGATIVE.format(
            "-2.0")  #"Довжина діагоналі ({}) не може бути менше або дорівнювати 0."
        self.assertEqual(output, expected_output)

    @patch('sys.stdout', new_callable=StringIO)
    @patch('builtins.input', side_effect=["abc","."])
    def test_main_non_numeric_input(self, mock_input, mock_stdout):
        with self.assertRaises(SystemExit) as cm:
            main()
        self.assertEqual(cm.exception.code, 1)

        output = mock_stdout.getvalue().strip()

        expected_output = ERROR_MESSAGE_NON_NUMERIC  # "Введене значення не є числом."
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()
