import sys
import math

ERROR_MESSAGE_NEGATIVE = "Довжина діагоналі ({}) не може бути менше або дорівнювати 0."
ERROR_MESSAGE_NON_NUMERIC = "Введене значення не є числом."

def calculate_square_area(v1):
    if v1 is None:
        return None
    return math.sqrt(v1 ** 2 / 2) * math.sqrt(v1 ** 2 / 2)

def handle_input_error():
    print(ERROR_MESSAGE_NON_NUMERIC)
    sys.exit(1)

def vvod():
    try:
        v1 = float(input("Введіть діагональ квадрату:"))
        if v1 <= 0:
            print(ERROR_MESSAGE_NEGATIVE.format(v1))
            sys.exit(1)
        return v1
    except ValueError:
        handle_input_error()

def main():
    try:
        v1 = vvod()
        v2 = calculate_square_area(v1)
        print(f"Площа квадрата з діагоналлю {v1} дорівнює {v2}.")
        sys.exit(0)
    except ValueError:
        sys.exit(1)


if __name__ == "__main__":
    main()
