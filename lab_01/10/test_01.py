import unittest
from unittest.mock import patch
from io import StringIO
import sys
import math
import task_01

class TestYourModule(unittest.TestCase):

    @patch('builtins.input', side_effect=["3.0", "4.0"])
    @patch('sys.stdout', new_callable=StringIO)
    def test_valid_inputs(self, mock_output, mock_input):
        with self.assertRaises(SystemExit) as se:
            task_01.main()
        self.assertEqual(se.exception.code, 0)  # Проверка кода выхода
        main_output = mock_output.getvalue().strip()
        self.assertEqual(main_output, f"Довжина гіпотенузи: {math.sqrt(3**2 + 4**2)}")
    @patch('builtins.input', side_effect=["-1.0", "-3.0"])
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_inputs(self, mock_output, mock_input):
        with self.assertRaises(SystemExit) as se:
            task_01.main()
        self.assertEqual(se.exception.code, 1)  # Проверка кода выхода
        main_output = mock_output.getvalue().strip()
        self.assertEqual(main_output, "Некоректні дані. Введіть додатнє число.")

    @patch('builtins.input', side_effect=["abc", "."])
    @patch('sys.stdout', new_callable=StringIO)
    def test_invalid_nonnumeric_inputs(self, mock_output, mock_input):
        with self.assertRaises(SystemExit) as se:
            task_01.main()
        self.assertEqual(se.exception.code, 1)  # Проверка кода выхода
        main_output = mock_output.getvalue().strip()
        self.assertEqual(main_output, "Некоректні дані. Введіть числові значення для довжин катетів.")

if __name__ == "__main__":
    unittest.main()