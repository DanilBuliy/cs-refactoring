import math
import sys

'''
Необхідно знайти Z1, Z2, Z3, якщо:
Z1=(sin(a)+2*sin(a-10))/(2*cos(a)-sin(3*a))
Z2=tan(3*a)/Z1
Z3=Z1+Z2
'''


def main():
    try:
        a = float(input("Введіть значення кута a в градусах: "))
        a = math.radians(a)  # Переводимо градуси в радіани
        n = math.sin(a) + 2 * math.sin(a - 10)
        if 2 * math.cos(a) - math.sin(3 * a) != 0:
            z1 = n / (2 * math.cos(a) - math.sin(3 * a))
            if isinstance(z1, float) and z1 != 0:
                z2 = math.tan(3 * a) / z1
                if isinstance(z1, float) and isinstance(z2, float):
                    z3 = z1 + z2
                    print(f"Z1 = {z1}")
                    print(f"Z2 = {z2}")
                    print(f"Z3 = {z3}")
                    sys.exit(0)
                else:
                    print("Некоректні дані. Значення Z1 та Z2 повинні бути числами.")# не можу зробити так, щоб виникла ця помилка
                    sys.exit(1)
            else:
                print("Некоректні дані. Значення Z1 повинно бути числом і не може дорівнювати нулю.")# не можу зробити так, щоб виникла ця помилка
                sys.exit(1)
        else:
            print("Некоректні дані. Знаменник не може дорівнювати нулю.")# не можу зробити так, щоб виникла ця помилка
            sys.exit(1)
    except ValueError:
        print("Некоректні дані. Введіть числове значення для кута a.")
        sys.exit(1)


if __name__ == "__main__":
    main()
